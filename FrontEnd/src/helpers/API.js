import axios from 'axios'
import apiconfig from '../configs/api.configs.json'


const API = {
    survey: async(id,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejala,perjalanan,riwayat,transprtasi,active,create_by,create_on,modify_by,modify_on,delete_by,delete_on)=>{
        let token=localStorage.getItem(apiconfig.LS.TOKEN)
        let option={
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.COMPANY,
            method: "GET",
            headers: {
                "Authorization": token
            },
            data:{
                id : id,
                email : email,
                partisipasi : partisipasi,
                nama : nama,
                umur : umur,
                no_hp : no_hp,
                status : status,
                kelurahan : kelurahan,
                kecamatan : kecamatan,
                kabupaten : kabupaten,
                fakultas : fakultas,
                jurusan : jurusan,
                gejala : gejala,
                perjalanan : perjalanan,
                riwayat : riwayat,
                transprtasi : transprtasi,
                active : active,
                create_by : create_by,
                create_on : create_on,
                modify_by : modify_by,
                modify_on : modify_on,
                delete_by : delete_by,
                delete_on : delete_on
            }
        }
        try {
            let result = await axios(option)
            return result.data
        } catch(error){
            return error.response.data
        }
    }
}

export default API
