import React from 'react'
import {Switch,Route} from 'react-router-dom'
import Dashboard from './components/Dashboard'
import apiconfig from './configs/api.configs.json'
class App extends React.Component{
    render(){
        return(
            <Switch>
                <Route exact path='/' render={()=>(
                    localStorage.getItem(apiconfig.LS.TOKEN)==null ? 
                    (<Route exact path='/' component={Dashboard}/>)
                :(<Dashboard/>))}/>
                <Dashboard/>
            </Switch>
        )
    }
}
export default App