import React from 'react'
import {Switch,Route} from 'react-router-dom'
import Header from './Header'
import Sidebar from './Sidebar'
import home from './content/home'
import list from './content/Survey/list'
import tambah from './content/Survey/tambah'
class Dashboard extends React.Component {
    render(){
        return (
            <div>
                <Header/>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar/>
                        <div class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                            
                            <Switch>
                                <Route path ="/home" component={home}/>
                                <Route path ="/survey" component={list}/>
                                <Route path ="/tambah" component={tambah}/>
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Dashboard