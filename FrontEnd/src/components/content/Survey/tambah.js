import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class tambah extends React.Component{
    constructor (props){
        super(props)
        this.state={
            formdata:{gejala1:false,gejala2:false,gejala3:false,gejala4:false,perjalanan1:false,perjalanan2:false,perjalanan3:false,perjalanan4:false,perjalanan5:false,perjalanan6:''
            ,riwayat1:false,riwayat2:false,riwayat3:false,riwayat4:false,transprtasi1:false,transprtasi2:false,transprtasi3:false,transprtasi4:false
            },
            opencheckbox:true,gejalaa:false}
        this. changeHandler = this. changeHandler.bind(this);
        this. cekHandler = this. cekHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this)
        this.enabledFakultas  = this.enabledFakultas.bind(this)
        this.disabledFakultas  = this.disabledFakultas.bind(this)
        this.enabledFakultas1  = this.enabledFakultas1.bind(this)
        this.disabledFakultas1  = this.disabledFakultas1.bind(this)
        this.enabledPerjalanan  = this.enabledPerjalanan.bind(this)
        this.enabledTransprtasi = this.enabledTransprtasi.bind(this)
    }
    cekHandler(e){
       if(document.getElementById(e.target.id).checked){
        let tmp = this.state.formdata
        tmp[e.target.name]=true
        this.setState({
            formdata:tmp
        })
       }else{let tmp = this.state.formdata
        tmp[e.target.name]=false
        this.setState({
            formdata:tmp
        })}
           
      
    }
    enabledFakultas(){
        document.getElementById("zz").disabled = false
    }
    disabledFakultas(){
        document.getElementById("zz").disabled = true
        document.getElementById("zz").value = ''
    }
    enabledPerjalanan(e){
      
        if(document.getElementById(e.target.id).checked){
            document.getElementById("perjalanan").disabled = false
            let tmp = this.state.formdata
            tmp[e.target.name]=true
            this.setState({
                formdata:tmp
            })
           }else{let tmp = this.state.formdata
            document.getElementById("perjalanan").disabled = true
            document.getElementById("perjalanan").value =''
            tmp[e.target.name]=false
            this.setState({
                formdata:tmp
            })}
    }
    enabledTransprtasi(e){
      
        if(document.getElementById(e.target.id).checked){
            document.getElementById("transportasi").disabled = false
            let tmp = this.state.formdata
            tmp[e.target.name]=true
            this.setState({
                formdata:tmp
            })
           }else{let tmp = this.state.formdata
            document.getElementById("transportasi").disabled = true
            document.getElementById("transportasi").value =''
            tmp[e.target.name]=false
            this.setState({
                formdata:tmp
            })}
    }
    enabledFakultas1(){
        document.getElementById("ff").disabled = false
    }
    disabledFakultas1(){
        document.getElementById("ff").disabled = true
        document.getElementById("ff").value = ''
    }
    componentDidMount(){
        this.Tanggal()
    }
    Tanggal(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
    
        var c = a+'-'+aa+'-'+aaa
        this.setState({
            Tanggal:c
        })
    }
    submitHandler(){
            this.state.formdata.create_on=this.state.Tanggal
            this.state.formdata.create_by='1'
            
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHSURVEY,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }
        let option2 = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHGEJALA,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }
        let option3 = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHPERJALANAN,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }
        let option4 = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHRIWAYAT,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }
        let option5 = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHTRANSPRTASI,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }



        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })

        axios(option2).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
        axios(option3).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
        axios(option4).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })

        axios(option5).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
    

}
    changeHandler(e){
        let tmp = this.state.formdata
        tmp[e.target.name]=e.target.value
        this.setState({
            formdata:tmp
        })
    }

    render(){
        return(<div class="container">
            {JSON.stringify(this.state.formdata)}
            <div class="card"> <h2>Surveilans Coronavirus Disease 2019 (COVID-19) Sivitas Akademika Universitas Jenderal Soedirman tahap II</h2>
            <div class="card-body">Sebagai bagian dari upaya kesiapsiagaan dalam menghadapi meluasnya penularan COVID 19, maka perlu dilakukan surveilans terutama pada sivitas akademika Unsoed.
Form Surveilans ini diinisiasi oleh Tim Tanggap Pandemi COVID-19 Unsoed, data yang dikumpulkan pada form ini akan dijaga kerahasiaannya dan digunakan HANYA UNTUK KEPENTINGAN SURVEILANS.
Untuk informasi lebih lanjut anda dapat menghubungi nomor pusat layanan COVID-19 Universitas Jenderal Soedirman melalui kontak telepon (0281) 641233 atau WA 081 229 366 919</div>
<div class="card-footer" style={{color:'#ff0000'}}>* Wajib</div></div>

<div class="card">
            <div class="card-body">Alamat email *
            <p><input type="text" class="form-control" name="email"
            value={this.state.formdata.email}  onChange={this.changeHandler}
                    reqiured placeholder="Email Anda"></input></p></div>
</div>


<div class="card"> 
            <div class="card-body">Form ini diisi berdasarkan kesukarelaan dan tidak terdapat paksaan. Dengan berpartisipasi mengisi form ini, anda menyatakan bersedia bahwa info kontak yang disediakan dapat dihubungi lebih lanjut oleh Tim Tanggap Pandemi Covid-19 Unsoed apabila dianggap perlu. Semua informasi bersifat rahasia, dikumpulkan serta dianalisis untuk kepentingan kesiapsiagan Unsoed menghadapi perkembangan infeksi Covid-19. Berikut ini saya menyatakan : *
            <p><div onChange={this.changeHandler} value={this.state.formdata.partisipasi}>
        <input type="radio" value="Bersedia" name={"partisipasi"} /> Bersedia
        <p>
        <input type="radio" value="Tolak" name={"partisipasi"} /> Tolak</p>
       
      </div></p></div></div>

      
<div class="card">
            <div class="card-body">Nama Lengkap: *
            <p><input type="text" class="form-control"
            value={this.state.formdata.nama}  onChange={this.changeHandler} name="nama"
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>

<div class="card">
            <div class="card-body">Umur: *
            <p><input type="text" class="form-control" name="umur"
            value={this.state.formdata.umur}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>

<div class="card">
            <div class="card-body">Nomor telepon yang dapat dihubungi: *
            <p><input type="text" class="form-control" name='no_hp'
            value={this.state.formdata.no_hp}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>

<div class="card"> 
            <div class="card-body">Status: *
            <p><div onChange={this.changeHandler} value={this.state.formdata.status}>
        <input type="radio" onClick={this.disabledFakultas}  value="Dosen" name="status" /> Dosen
        <p>
        <input type="radio" onClick={this.disabledFakultas}  value="Tenaga Kependidikan" name="status" /> Tenaga Kependidikan</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas}  value="Cleaning Service" name="status" /> Cleaning Service</p>
        <p>
        <input type="radio"onClick={this.disabledFakultas}  value="Security" name="status" /> Security</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas} value="Mahasiswa" name="status" /> Mahasiswa</p>
        <p>
        <input type="radio"  onClick={this.enabledFakultas} for="zz" value={this.state.formdata.status} name="status" /> Yang Lain : 
        <input type="text"  id="zz" class="form-control"
                    name="status"
                    onChange={this.changeHandler}
                   disabled
                    reqiured placeholder="Jawaban Anda"></input></p>
       
      </div></p></div></div>

      <div class="card">
            <div class="card-body">Alamat tempat tinggal saat ini (Kelurahan): *
            <p><input type="text" class="form-control"name="kelurahan"
            value={this.state.formdata.kelurahan}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>

<div class="card">
            <div class="card-body">Alamat tempat tinggal saat ini (Kecamatan): *
            <p><input type="text" class="form-control"name="kecamatan"
            value={this.state.formdata.kecamatan}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>

<div class="card">
            <div class="card-body">Alamat tempat tinggal saat ini (Kabupaten): *
            <p><input type="text" class="form-control"name="kabupaten"
            value={this.state.formdata.kabupaten}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>


<div class="card"> 
            <div class="card-body">Fakultas: *
            <p><div onChange={this.changeHandler} value={this.state.formdata.fakultas}>
        <input type="radio" onClick={this.disabledFakultas1} value="Pertanian" name="fakultas" /> Pertanian
        <p>
        <input type="radio" onClick={this.disabledFakultas1} value="Bilogi" name="fakultas" /> Bilogi</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas1} value="Ekonomi dan bisnis" name="fakultas" /> Ekonomi dan bisnis</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas1} value="Perternakan" name="fakultas" /> Perternakan</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas1} value="Hukum" name="fakultas" /> Hukum</p>
        <p>
        <input type="radio" onClick={this.disabledFakultas1} value="Teknik" name="fakultas" /> Teknik</p>
        <p>   
        <input type="radio" onClick={this.enabledFakultas1} value={this.state.formdata.fakultas} name="fakultas" /> Yang Lain : 
        <input id="ff"type="text" class="form-control"
                    name="fakultas"
                    disabled
                    onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p>
       
      </div></p></div></div>

      <div class="card">
            <div class="card-body"> Jurusan/ Departemen/ Program Studi/ Subunit kerja *
            <p><input type="text" class="form-control"name="jurusan"
            value={this.state.formdata.jurusan}  onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p></div>
</div>


<div class="card"> 
            <div class="card-body">Apakah anda sedang mengalami gejala seperti berikut dalam 14 hari terakhir (boleh memilih lebih dari satu) ? *
            <p><div onChange={this.cekHandler}  >
        <input type="checkbox"  id="gejala1" value={!this.state.formdata.gejala1} name="gejala1" /> Demam/riwayat demam dalam 14 hari terakhir
        <p>
        <input type="checkbox" id="gejala2" value={this.state.formdata.gejala2} name="gejala2" /> Batuk/pilek/nyeri tenggorokan</p>
        <p>
        <input type="checkbox" id="gejala3" value={this.state.formdata.gejala3} name="gejala3" /> Sesak Napas</p>
        <p>
        <input type="checkbox" id="gejala4" value={this.state.formdata.gejala4} name="gejala4" /> Saya tidak mengalami keluhan seperti di atas</p>
      </div></p></div></div>

<div class="card"> 
            <div class="card-body">Apakah anda memiliki faktor risiko melakukan perjalanan ke luar negeri atau kota-kota terjangkit di Indonesia dalam waktu 14 hari terakhir (boleh memilih lebih dari satu)? *
            <p><div >
        <input  onChange={this.cekHandler} type="checkbox" id="perjalanan1" value={this.state.formdata.perjalanan1} name="perjalanan1" /> Jakarta
        <p>
        <input  onChange={this.cekHandler} type="checkbox" id="perjalanan2" value={this.state.formdata.perjalanan2} name="perjalanan2" /> Bandung</p>
        <p>
        <input onChange={this.cekHandler}  type="checkbox" id="perjalanan3" value={this.state.formdata.perjalanan3} name="perjalanan3" /> Yogya</p>
        <p>
        <input  onChange={this.cekHandler} type="checkbox" id="perjalanan4" value={this.state.formdata.perjalanan4} name="perjalanan4" /> Saya tidak memiliki riwayat perjalanan ke luar negeri maupun kota yang terjangkit</p>
        <p>   
        <input   onChange={this.enabledPerjalanan}  type="checkbox" id="perjalanan5" value={this.state.formdata.perjalanan5} name="perjalanan5" /> Yang Lain : 
        <input  id="perjalanan" type="text" class="form-control"
                    name="perjalanan6"
                    value={this.state.formdata.perjalanan6}
                    disabled
                    onChange={this.changeHandler}
                    reqiured placeholder="Jawaban Anda"></input></p>
      </div></p></div></div>


<div class="card"> 
            <div class="card-body">Apakah anda memiliki riwayat paparan faktor risiko berikut dalam 14 hari terakhir? (boleh memilih lebih dari satu) *
            <p><div onChange={this.cekHandler} >
        <input type="checkbox"  id="riwayat1" value={this.state.formdata.riwayat1} name="riwayat1" /> Riwayat kontak erat dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19
        <p>
        <input type="checkbox"  id="riwayat2" value={this.state.formdata.riwayat2} name="riwayat2" /> Bekerja atau mengunjungi fasilitas kesehatan yang berhubungan dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19</p>
        <p>
        <input type="checkbox"  id="riwayat3" value={this.state.formdata.riwayat3} name="riwayat3" /> Memiliki demam (lebih dari 38 derajat C) atau ada riwayat demam dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar negeri atau kontak dengan orang yang memiliki riwayat perjalanan ke luar negeri</p>
        <p>
        <input type="checkbox"  id="riwayat4" value={this.state.formdata.riwayat4} name="riwayat4" /> Saya tidak memiliki riwayat perjalanan ke luar negeri maupun kota yang terjangkit</p>
      </div></p></div></div>

      <div class="card"> 
            <div class="card-body">Transportasi umum yang biasa digunakan dalam kurun waktu 14 hari terakhir (boleh memilih lebih dari satu) *
            <p><div >
        <input onChange={this.changeHandler} type="checkbox" value={this.state.formdata.transprtasi1} id="transprtasi1"name="transprtasi1" /> Kendaraan pribadi
        <p>
        <input onChange={this.changeHandler} type="checkbox" value={this.state.formdata.transprtasi2} id="transprtasi2"name="transprtasi2" /> Bus</p>
        <p>
        <input onChange={this.changeHandler} type="checkbox" value={this.state.formdata.transprtasi3} id="transprtasi3"name="transprtasi3" /> Angkot</p>
        <p>   
        <input onChange={this.enabledTransprtasi} type="checkbox" value={this.state.formdata.transprtasi4} id="transprtasi4"name="transprtasi4" /> Yang Lain : 
        <input type="text"  id="transportasi"class="form-control"
                    name="transprtasi5"
                    value={this.state.formdata.trnsprtasi5}
                    onChange={this.changeHandler}
                    disabled
                    reqiured placeholder="Jawaban Anda"></input></p>
      </div></p></div></div>
<button onClick={this.submitHandler}>Kirim</button>
<div class="footer">Jangan pernah mengirimkan sandi melalui Google Formulir.
<p><center>Formulir ini dibuat dalam Universitas Jenderal Soedirman. Laporkan Penyalahgunaan</center></p></div>
</div>


        )
    }
}
export default tambah