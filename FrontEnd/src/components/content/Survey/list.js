import React from 'react'
import apiconfig from '../../../configs/api.configs.json'
import {Row, Container} from 'reactstrap'
import axios from 'axios'
import {Link} from 'react-router-dom'
import Hapus from './hapus'

class list extends React.Component{
    constructor(props){
        super(props)
        this.state={resource:[], hapusList:false, currentList:{}
        }
        this.closeModalHandler = this.closeModalHandler.bind(this)
        this.hapusModalHandler = this.hapusModalHandler.bind(this)
    }

    componentDidMount(){
        this.getListResource()
    }
    getListResource(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.SURVEY,
            method:"get",
            header:{
                "Authorization": token
            }
        }
        axios(option).then((response)=>{
            this.setState({
                resource: response.data.message
            })
        }).catch((error)=>{
            alert(error)
        })
    }
    hapusModalHandler(kodelist){
        let tmp = {}
        this.state.resource.map((row)=>{
            if(kodelist == row.id){
                tmp = row
            }
        })
        this.setState({
            hapusList:true,
            currentList : tmp
            
        })
    }
    closeModalHandler(){
        this.setState({
            hapusList:false
        })
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()
        this.getListResource()}
    render (){
        return(
            <Container>
            <Hapus 
        coba = {this.state.currentList}
        hapus = {this.state.hapusList}
        closeModalhandler = {this.closeModalHandler}
        />
            <Row><button>
            <a href="/tambah" >
                          <i class="far nav-icon"></i>
                          <p>Tambah</p>
                        </a> </button>
            <table style={{borderStyle:'insert'}}id="mytable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    </tr></thead> 
                    <tbody>
                        <td>Nama</td>
                        <td>Email</td>
                        <td>Action</td>
                        {
                           this.state.resource.map((row)=>
                        <tr><td>{row.nama}</td>
                            <td>{row.email}</td>
                        <td><button> Edit</button> <button onClick={()=>{this.hapusModalHandler(row.id)}}>Delete</button></td>
                        <td>
                                          <Link to='#'>
                                          </Link></td>
                           </tr>)
                        }
                        
                        </tbody>
           </table></Row></Container>
        )
    }
}export default list