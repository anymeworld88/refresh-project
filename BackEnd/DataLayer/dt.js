const pg = require('pg')
const DatabaseConnection = require('../Config/dbp.config.json')
var DB = new pg.Pool(DatabaseConnection.config)
const bcrypt = require('bcryptjs')
const dt ={
    readSurveyAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select * from refresh_db where active=true',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    hapusListAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update refresh_db set delete_by=($1),delete_on=($2),active=($3) where id=($4)',
                values:[docs.delete_by,docs.delete_on,docs.active,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Delete"]}
               
                callback(data)
            })
        })
    },
    tambahClientAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into refresh_db (create_by,create_on,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejalaId,perjalananId,riwayatId,transprtasiId,active) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,true)',
                values:[docs.create_by,docs.create_on,docs.email,docs.partisipasi,docs.nama,docs.umur,docs.no_hp,docs.status,docs.kelurahan,docs.kecamatan,docs.kabupaten,docs.fakultas,docs.jurusan,docs.gejala,docs.perjalanan,docs.riwayat,docs.transprtasi,docs.active]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    tambahGejalaAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into refresh_db (create_by,create_on,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejalaId,perjalananId,riwayatId,transprtasiId,active) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,true)',
                values:[docs.id,docs.create_by,docs.create_on,docs.email,docs.partisipasi,docs.nama,docs.umur,docs.no_hp,docs.status,docs.kelurahan,docs.kecamatan,docs.kabupaten,docs.fakultas,docs.jurusan,docs.gejala,docs.perjalanan,docs.riwayat,docs.transprtasi,docs.active]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    tambahPerjlananAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into refresh_db (create_by,create_on,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejalaId,perjalananId,riwayatId,transprtasiId,active) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,true)',
                values:[docs.create_by,docs.create_on,docs.email,docs.partisipasi,docs.nama,docs.umur,docs.no_hp,docs.status,docs.kelurahan,docs.kecamatan,docs.kabupaten,docs.fakultas,docs.jurusan,docs.gejala,docs.perjalanan,docs.riwayat,docs.transprtasi,docs.active]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    tambahRiwayatAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into refresh_db (create_by,create_on,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejalaId,perjalananId,riwayatId,transprtasiId,active) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,true)',
                values:[docs.create_by,docs.create_on,docs.email,docs.partisipasi,docs.nama,docs.umur,docs.no_hp,docs.status,docs.kelurahan,docs.kecamatan,docs.kabupaten,docs.fakultas,docs.jurusan,docs.gejala,docs.perjalanan,docs.riwayat,docs.transprtasi,docs.active]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    tambahTransprtasiAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into refresh_db (create_by,create_on,email,partisipasi,nama,umur,no_hp,status,kelurahan,kecamatan,kabupaten,fakultas,jurusan,gejalaId,perjalananId,riwayatId,transprtasiId,active) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,true)',
                values:[docs.create_by,docs.create_on,docs.email,docs.partisipasi,docs.nama,docs.umur,docs.no_hp,docs.status,docs.kelurahan,docs.kecamatan,docs.kabupaten,docs.fakultas,docs.jurusan,docs.gejala,docs.perjalanan,docs.riwayat,docs.transprtasi,docs.active]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    }
}
module.exports = dt