const SurveyLogic = require('../Survey/Survey_BL.js')
module.exports = exports = function(server){
    server.get('/api/listsurvey',SurveyLogic.readSurveyAllHandler),
    server.put('/api/hapuslist',SurveyLogic.hapusListAllHandler),
    server.post('/api/tambahsurvey',SurveyLogic.tambahClientAllHandler),
    server.post('/api/tambahgejala',SurveyLogic.tambahGejalaAllHandler),
    server.post('/api/tambahperjalanan',SurveyLogic.tambahPerjalananAllHandler),
    server.post('/api/tambahriwayat',SurveyLogic.tambahRiwayatAllHandler),
    server.post('/api/tambahtransprtasi',SurveyLogic.tambahTransprtasiAllHandler)


}